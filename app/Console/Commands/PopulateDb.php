<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use Storage;
use Faker;
use Faker\Factory;

class PopulateDb extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'InitUserDl';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Downloads User info from JSON file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //lets use faker to generate passwords
        $faker = Faker\Factory::create();

        $list = json_decode( Storage::disk('local')->get('quiz1.json') );
        $progress = $this->output->createProgressBar( count( $list ) );

        try {

            $this->info('Adding data...');

            //lets loop though the list of data
            foreach( $list as $user ) {

                //lets strip the scheme from the url to save into db for security reasons
                $url = parse_url( $user->blog );

                //lets check if the host exists in the url
                if( array_key_exists( 'host', $url ) ) {
                    $blog = $url['host'] . $url['path'] . $url['query'];
                } else {
                    $blog = '';
                }

                //lets generate user data from the json file
                User::create([
                    'first_name' => $user->first_name,
                    'last_name' => $user->last_name,
                    'username' => $user->username,
                    'email' => $user->email,
                    'password' => $faker->password,
                    'city' => $user->city,
                    'fav_color' => $user->fav_color,
                    'blog' => $blog,
                    'country' => $user->country,
                    'title' => $user->title
                ]);

                $progress->advance();

            }

            $progress->finish();
            $this->line('');
            $this->info('User data added to database!');

        } catch(Exception $e) {
            $this->error( 'Uh Oh! Error:' . $e->getMessage() );
        }

    }
}
