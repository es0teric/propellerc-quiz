<?php namespace App\Http\Controllers;

use App\Http\Requests;
use Storage, Input, Response, Request;
use App\User;

class SearchController extends Controller
{
    /**
     * Index for search page
     *
     * @return Illuminate\View\View
     */
    public function index() {
        return view('search')->with('title', 'Search Page');
    }

    /**
     * Searches users by username and returns json response
     *
     * @return Illuminate\Http\Response
     */
    public function searchUsers() {

        $query = filter_var( Input::get('query'), FILTER_SANITIZE_STRING );

        $items = User::where('username', 'like', '%' . $query . '%')->get(['id', 'username', 'first_name', 'last_name', 'title']);

        return Response::json($items);

    }

    /**
     * Shows user data based on ID
     *
     * @param  int $id  user ID in database
     * @return Illuminate\Http\Response
     */
    public function show($id) {

        $user = User::find($id);

        return Response::json($user);

    }
}
