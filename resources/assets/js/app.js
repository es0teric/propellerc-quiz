$(document).ready(function() {

    //sets autocomplete for search
    $('#search').autocomplete({

        serviceUrl: '/user/search',
        minChars: 2,
        onSearchComplete: function( query, suggestions ) {

            //lets check if there are any results
            if( suggestions.length == 0 ) {

                $(this).css('border-bottom', '1px solid red');
                $('#error').fadeToggle('medium');

                $('#error').append(
                    $('<h1/>').text('User not found!')
                );

                //removes duplicates of the same h1 error message
                if( $('#error h1').length > 1 )
                    $('#error h1').prev().remove();

            } else {

                $(this).css('border-bottom', '1px solid green');

            }

        },
        transformResult: function(response) {

            // Parse the output
            response = JSON.parse(response);

            return {
                //parses suggestions to be readable by autocomplete
                suggestions: $.map(response, function(dataItem) {

                    var query = $('#search').val();

                    //checks if user has title
                    if( dataItem.title )
                        var title = dataItem.title;
                    else
                        var title = '';

                    // Format appropriatelyA
                    return {
                        value: dataItem.first_name + " " + dataItem.last_name + " " + title,
                        data: {
                            id: dataItem.id,
                            username: dataItem.username,
                            firstName: dataItem.first_name,
                            lastName: dataItem.last_name,
                            title: dataItem.title
                        }
                    }

                })
            }

        },
        onSelect: function(suggestion) {

            $.ajax({
                url: '/user/' + suggestion.data.id,
                dataType: 'json'
            }).done(function( data ) {

                console.log( data );

                //lets format the date and time
                var date = moment( data.created_at ).format('MMMM Do, YYYY');

                //construct result list template with data
                $('#result').append(
                    $('<ul/>', { class: 'resultList' })
                    .css({
                        'background-color': data.fav_color
                    }).append(
                        $('<li/>').append(
                            $('<p/>').text( 'Email: ' + data.email )
                        )
                    ).append(
                        $('<li/>').append(
                            $('<p/>').append(
                                $('<a/>', { href: "http://" + data.blog }).text('Blog')
                            )
                        )
                    ).append(
                        $('<li/>').append(
                            $('<p/>').text( 'City: ' + data.city )
                        )
                    ).append(
                        $('<li/>').append(
                            $('<p/>').text( 'Country: ' + data.country )
                        )
                    ).append(
                        $('<li/>').append(
                            $('<p/>').text( 'Member since: ' + date )
                        )
                    )
                );

                if( $('#result ul').length > 1 )
                    $('#result ul').prev().remove();

            });

        }
    });

});
