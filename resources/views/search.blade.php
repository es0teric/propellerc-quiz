@extends('layouts.global')

@section('content')

    {{ Form::open(['action' => 'SearchController@searchUsers', 'method' => 'get', 'class' => 'searchForm' ]) }}
        <div class="form-group">
            <i class="fa fa-search fa-lg" aria-hidden="true"></i>
            <input type="text" id="search" name="q" placeholder="search for user" />
        </div>

    {{ Form::close() }}

    <div id="error"></div>
    <div id="result"></div>

@stop
