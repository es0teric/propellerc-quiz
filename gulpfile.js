var gulp = require('gulp'),
	sass = require('gulp-sass'),
	gutil = require('gulp-util'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	rename = require('gulp-rename'),
	livereload = require('gulp-livereload'),
	notify = require('gulp-notify'),
	minifycss = require('gulp-minify-css'),
	watch = require('gulp-watch'),
	browserSync = require('browser-sync').create();

var config = {
	sassRoot: './resources/assets/sass',
	jsRoot: './resources/assets/js',
	bourbon: './resources/assets/sass/bourbon/app/assets/stylesheets',
	bitters: './resources/assets/sass/bitters/app/assets/stylesheets',
	neat: './resources/assets/sass/neat/app/assets/stylesheets',
	fontawesome: './resources/assets/sass/font-awesome/scss'
};

gulp.task('css', function() {

	return gulp.src( config.sassRoot + '/app.scss' )
		.pipe(
			sass({
				style:'compressed',
				loadPath: [
					'./resources/assets/sass',
					config.bourbon + '_bourbon.scss',
					config.bitters + '_base.scss',
					config.neat
				]
			})
		)
		.on("error", notify.onError(function (error) {
          	return "Error: " + error.message;
          }))
		.pipe( minifycss() )
		.pipe(
			rename({
				suffix: '.min'
			})
		)
		.pipe( gulp.dest( './public/assets/css' ) )
		.pipe(
			notify({
				message: 'CSS compiled successfully!'
			})
		); 
});

gulp.task( 'compile-js', function() {

	return gulp.src( config.jsRoot + "/*.js" )
		.pipe( uglify().on('error', gutil.log ) )
		.pipe(
			rename({
				suffix: '.min'
			})
		)
		.pipe( gulp.dest('./public/assets/js') )
		.pipe(
			notify({
				message: "Js compiled and minified!"
			})
		)

});

gulp.task('init', ['css', 'compile-js'], function() {

	//initialize browserSync
	browserSync.init({
      proxy: "http://propeller-quiz.dev/",
		files: [
			config.sassRoot + '/*.scss',
			{
				match: [
					'./resources/views/layouts/*.php',
					'./resources/views/partials/*.php',
					'./resources/views/*.php',
					'./resources/views/errors/*.php',
					'./resources/views/emails/*.php'
				],
				fn: function( event, file ) {
					this.reload();
				}
			}
		]
	});

	gulp.watch( config.jsRoot + '/*.js', ['compile-js'] )
		.on( 'change', function() {
			browserSync.reload();
			notify( 'Js compiled!' ).write('');
		});

	gulp.watch( config.sassRoot + '/*.scss', ['css'] )
		.on('change', function() {
			browserSync.reload();
			notify('CSS compressed from root scss & browser reloaded!').write('');
		});

});
